#!/usr/local/bin/python3

""" Launch Stargazing Application """

import os
import lib
import time


def first_option():
    """ Get hourly rating for stargazing for the next 5 days """
    header()
    print('THIS IS FIRST OPTION')


def second_option():
    header()
    print('THIS IS SECOND OPTION')


def main():
    option = menu()
    while option != 9:
        if option == 1:
            first_option()

            input('\nPress ENTER to return to the menu >> ')
        elif option == 2:
            second_option()

            input('\nPress ENTER to return to the menu >> ')
        else:
            print("Answer not valid !\nPlease enter a new option...")
            time.sleep(1)

        option = menu()
    if option == 9:
        print('Goodbye..')


def header():
    os.system('clear')
    print('Stargazing Application (ESC -> Main Menu)')
    print('----------------------\n')


# Main Menu
def menu():
    """ Ask User which program does he want to open.

    Return
    ------
    int: User's choice """
    header()

    print("1) Tonight's rating")
    print("2) Hourly forecast for the 5 next days")
    print("3) Daily forecast for the 16 next days")

    print(foreground.red + "\n\n9) Quit" + color.reset)

    option = input('\n\n\n\n>> ')
    try:
        option = int(option)
    except ValueError:
        print("Answer not valid !\nPlease enter a new option...")
        time.sleep(1)
        menu()
    return option


if __name__ == '__main__':
    color = lib.colors
    foreground = lib.foreground
    background = lib.background

    main()
