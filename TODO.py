import json
import pytz
import datetime
import ephem
import pylunar
import math


def sun():
    # get timezone of LocationInfo : city.timezone (if tz was string) or city.timezone.zone (if tz was pytz)
    # elevation function doesn't really works (need to look more into it)
    # time_at_elevetion works fine
    from astral import LocationInfo, Depression, SunDirection
    from astral.sun import sun, time_at_elevation
    city = LocationInfo("Quebec", "Canada",
                        pytz.timezone('America/Toronto'), 46.813135, -71.227587)

    astro_depression = Depression.ASTRONOMICAL

    s = sun(city.observer, date=datetime.date(2021, 1, 18), dawn_dusk_depression=astro_depression,
            tzinfo=pytz.timezone('America/Toronto'))

    time = time_at_elevation(city.observer, -18, datetime.datetime(
        2021, 1, 17), tzinfo=pytz.timezone('America/Toronto'), direction=SunDirection.SETTING)

    def get_highest_alt(observer):
        eleptic_angle = 23.44
        max_alt = 90
        lat = 46.813135
        alt = round(max_alt + eleptic_angle - lat, 1)
        while True:
            try:
                time = time_at_elevation(observer, alt, datetime.datetime(
                    2021, 1, 23), tzinfo=pytz.timezone('America/Toronto'))
                break
            except:
                alt -= 0.1
        return alt, time

    def pysolar(self, datetime):
        from pysolar import solar
        date = self.weather.timezone.localize(datetime)
        alt = solar.get_altitude(self.weather.lat, self.weather.lon, date)
        return alt


def timezones():
    # Countries and timezones data
    timezone_countries = {timezone: country
                          for country, timezones in pytz.country_timezones.items()
                          for timezone in timezones}

    with open('country_by_timezone.json', 'w') as file:
        json.dump(timezone_countries, file)

    # Is currently in dst by timezone
    bool(datetime.datetime.now(pytz.timezone('America/Toronto')).dst())


def truncate(f, n):
    return math.floor(f * 10 ** n) / 10 ** n


def moon_illumination():
    y = 2021
    m = 1
    d = 7
    h = 7
    mi = 0
    sec = 0
    date = f'{y}/{str(m).zfill(2)}/{str(d).zfill(2)} {str(h).zfill(2)}:{str(mi).zfill(2)}:{str(sec).zfill(2)}'

    # Ephem Lib
    ep_moon = ephem.Moon()
    ep_moon.compute(date)
    phase = round(ep_moon.moon_phase*100, 1)

    # PyLunar lib
    pyl_moon = pylunar.MoonInfo(
        (46, 48, 46.517), (-71, 13, 38.567), ephem.Moon)
    pyl_moon.update((y, m, d, h + 5, mi, sec))
    pyl_phase = round(pyl_moon.fractional_phase()*100, 1)
    riseandset = pyl_moon.rise_set_times('America/Toronto')


def get_current_location():
    import geocoder
    g = geocoder.ip('me')
    lat = g.latlng[0]
    long = g.latlng[1]

    return lat, long


def getLocation(adress):
    from geopy.geocoders import Nominatim
    from geopy.location import _location_tuple

    geolocator = Nominatim(user_agent="weatherforecast")
    location = geolocator.geocode(adress)
    lat = location.latitude
    long = location.longitude

    return lat, long


def sky(lat, lon):
    from requests import request
    from math import log10

    def bortle_scale(brightness):

        sqm = log10((brightness + 0.171168465)/108000000)/-0.4

        if sqm > 23 or sqm < 5:
            return ValueError("Sky brightness (SQM) cannot be higher than 23 or less than 5.")

        bortle_sky = {
            1: [23, 21.99],
            2: [21.99, 21.89],
            3: [21.89, 21.69],
            4: [21.69, 21.25],
            4.5: [21.25, 20.49],
            5: [20.49, 19.50],
            6: [19.50, 18.95],
            7: [18.95, 18.38],
            8: [18.38, 17.80],
            9: [17.80, 5]
        }

        for bortle, sky in bortle_sky.items():
            if sqm <= sky[0] and sqm >= sky[1]:
                return bortle

    url = "https://www.lightpollutionmap.info/QueryRaster/"

    querystring = {"ql": 'wa_2015', "qt": 'point',
                   "qd": f'{lon},{lat}', "key": "078BDzVxU2IwEaGc"}
    response = request(
        "GET", url, params=querystring)

    brightness = float(response.text)

    bortle = bortle_scale(brightness)
    ratio = round(brightness / 0.171168465, 1)
    arti = round(brightness * 1000)
    print(f"{arti} μcd/m2")


sky(46.82463, -71.24684)

#
# My lat,lon = 46.813135, -71.227587
#
#
# TODO: Option 1
# Get Hourly Rating for the next 5 days
# Get all data
# Find an algorithm
#
#
# TODO: Option 2
#
# TODO: Option 3
#
# TODO: Adding elevation to the rating function (see elevation function above)
#
# TODO: Add ability to get moon / sun (etc.) infos for a certain date
#
# TODO: Add day/night period (dawn, Nautical dawn, Astronomical dawn, etc.) and same for dusk
# https://en.wikipedia.org/wiki/Dawn
# https://en.wikipedia.org/wiki/Dusk
# TODO: Bind ESC to main()
# TODO: Add ascii art at beggining
#
# TODO: When forecast == 16, l'intensité de la lune est calculé à 20h chaque jour
# Pour le calcul de l'intensité, prendre en compte été/hiver
# TODO: Rating -> Add altitude of target: altitude above horizon
#                  distance from moon: distance between moon and target

# FIX: Catch l'erreur de json.load() quand fichier vide ou inexistant

# data needed :
# clouds: cloud coverage
# humidity: quantity of water particules in the air
# transparency: clarity of the atmosphere (dust, moisture)
# seeing: steadiness of the atmosphere (twinkling stars, )
# dew point: temperature at wich water condensed (best when < 3°C below T° with < 60% humidity)
# probability of precipitation: probability of rain

# moon intensity: pourcentage of disk illuminated
# sun altitude: altitude below horizon (best when >= 18°)
# moon altitude: altitude (the greater moon intensity, the lower moon altitude)

# Website for a lot of astronomy / weather informations
# Dark hours, etc.
# https://www.timeanddate.com/moon/phases/
