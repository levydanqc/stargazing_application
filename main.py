# For multi-threaded execution (multiple script at once)
from multiprocessing.pool import ThreadPool
import time
from kivy.app import App
from kivy.properties import ObjectProperty
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.config import Config
Config.set('kivy', 'exit_on_escape', '1')
Config.set('kivy', 'desktop', '0')
Config.set('graphics', 'resizable', '0')
Config.set('graphics', 'position', 'auto')
Config.set('graphics', 'width', '381')
Config.set('graphics', 'height', '825')


class MenuScreen(Screen):
    pass


class ClockScreen(Screen):
    pass


class CalendarScreen(Screen):
    pass


class Manager(ScreenManager):
    menu_screen = ObjectProperty(None)
    clock_screen = ObjectProperty(None)
    calendar_screen = ObjectProperty(None)


class AstroCast(App):
    from lib import Weather
    hourlyWeather = Weather(46.76079, -71.35730, 5)
    # self.dailyWeather = Weather(46.76079, -71.35730, 16)

    def build(self):
        return Manager()


if __name__ == "__main__":
    AstroCast().run()
