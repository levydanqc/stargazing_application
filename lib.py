from json import load, dump
from datetime import datetime, timedelta
from typing import Tuple
from pylunar import MoonInfo
from ephem import Moon
from requests import request
from pytz import timezone
from astral import LocationInfo, Depression
from astral.sun import sun
from srtm import get_data
from own_packages.color_terminal import colors
from math import log10

colors = colors.Colors()
foreground = colors.fg()
background = colors.bg()


class Weather:
    def __init__(self, lat: float, lon: float, period: int):
        if period not in [5, 16]:
            raise ValueError('Error in period, value must be 5 or 16.')

        self.period = period
        self.lat = lat
        self.lon = lon
        self.elevation = self.get_elevation()
        self.timezone = timezone('America/Toronto')
        self.timezone_name = self.timezone.tzname(None)
        self.utc_time = datetime.utcnow().timetuple()
        local_time = datetime.now(
            self.timezone)
        dst = local_time.dst()

        self.local_time = local_time.timetuple()
        self.localtimeStr = local_time.strftime("%A %d %B")
        self.is_dst = bool(dst)
        self.didReset = False

        file = self.load_forecast()

        self.load_properties(file)

        self.astro = Astro(self)

    def load_forecast(self):
        """ Load the last weather conditions retrieved without having
        to use the api key. """
        with open(f'forecast{self.period}.json') as json_file:
            file = load(json_file)

            timestamp = file.get('last_update')
            lyear = int(timestamp.split(' ')[0].split('-')[0])
            lmonth = int(timestamp.split(' ')[0].split('-')[1])
            lday = int(timestamp.split(' ')[0].split('-')[2])
            lhour = int(timestamp.split(' ')[1].split(':')[0])
            llat = file.get('lat')
            llon = file.get('lon')

        if (lyear < self.local_time[0] or lmonth < self.local_time[1] or
            lday < self.local_time[2] or round(self.lat, 2) != llat or
                round(self.lon, 2) != llon or lhour+12 <= self.local_time[3]):
            file = self.get_forecast()
            self.didReset = True

        return file

    def get_forecast(self):
        """ Get weather conditions forecast hourly for the next 5 days
            or daily for the next 16 days.
            From Weather Bit through RapidApi. """

        headers = {
            'x-rapidapi-host': "weatherbit-v1-mashape.p.rapidapi.com",
            'x-rapidapi-key': "767eb4f027mshb8a874d688a3b4ep155710jsne4d64b15e783"
        }

        if self.period == 5:
            url = "https://rapidapi.p.rapidapi.com/forecast/hourly"

            querystring = {"lat": self.lat, "lon": self.lon,
                           "lang": "en", "hours": "120", "units": "M"}
        elif self.period == 16:
            url = "https://rapidapi.p.rapidapi.com/forecast/daily"

            querystring = {"lat": self.lat, "lon": self.lon,
                           "lang": "en", "units": "M"}

        response = request(
            "GET", url, headers=headers, params=querystring)

        text = response.text.replace("null", "None")

        file = eval(text)

        date = f'{self.local_time[0]}-{str(self.local_time[1]).zfill(2)}-{str(self.local_time[2]).zfill(2)} {str(self.local_time[3]).zfill(2)}:{str(self.local_time[4]).zfill(2)}:{str(self.local_time[5]).zfill(2)}'
        file['last_update'] = date

        with open(f'forecast{self.period}.json', 'w') as outfile:
            dump(file, outfile)

        return file

    def get_elevation(self):
        elevation_data = get_data()
        elevation = elevation_data.get_elevation(self.lat, self.lon)
        return elevation

    def toDMS(self, decimal: float) -> Tuple[int, int, float]:
        """ Convert decimal degree to degree, minute, seconde notation.

        Parameters
        ----------
        decimal: float
            decimal degree to convert

        Returns
        -------
        Tuple[degree: int, minute: int, sec: float]:
            tuple of degree, minute, seconde
        """
        deg = int(decimal)
        mi = int((decimal - deg) * 60)
        sec = (decimal - deg - mi/60) * 3600

        return (deg, mi, sec)

    def toDegree(self, dms: Tuple[int, int, float]) -> float:
        """ Convert degree, minute, seconde to decimal notation.

        Parameters
        ----------
        dms : Tuple[degree: int, minute: int, sec: float]
            tuple of degree, minute, seconde

        Returns
        -------
        float
            decimal degree notation
        """

        return dms[0] + dms[1]/60 + dms[2]/3600

    def load_properties(self, file):
        """ Load weather properties inside Weather class attributes. """

        self.city_name = file.get('city_name')
        self.country_code = file.get('country_code')
        self.timezone = file.get('timezone')
        self.last_update = file.get('last_update')

        if self.period == 5:
            self.forecast = self.load_hours(file)
        elif self.period == 16:
            self.forecast = self.load_days(file)

    def load_hours(self, file):
        """ Load hours weather forecast informations for the next 5 days.

            Returns
            -------
            dict:
                datetime <str>: time and date,
                datetime_utc <str>: time and date in utc time,
                clouds <int>: total clouds coverage (%),
                clouds_hi <int>: high altitude (Stratus) clouds coverage (%),
                clouds_mid <int>: middle altitude (Cumulus) clouds coverage (%),
                clouds_low <int>: low altitude (Cirrus) clouds coverage (%),
                wind_dir <str>: wind direction (in Cardinal point),
                wind_speed <float>: wind speed (m/s),
                humidity <int>: relative humidity (%),
                pop <int>: probability of precipitation (%),
                precipitation <float>: precipitation (mm),
                dewpoint <float>: dew point temperature (°C),
                vis <float>: visibility (km),
                snow <float>: snowfall (mm),
                app_temp <float>: apparent 'feels like' temperature (°C),
                temp <float>: temperature (°C),
                description <str>: quick weather description,
                weather_code <int>: weather code for icon
        """
        data = file.get('data')

        days = []
        day = []

        for i, hour in enumerate(data):
            if hour.get('timestamp_local').split('T')[1].split(':')[0] == '00' and i != 0:
                days.append(day[:])
                day.clear()

            day.append(
                {
                    'datetime': hour.get('timestamp_local').replace('T', ' '),
                    'datetime_utc': hour.get('timestamp_utc'),
                    'clouds': hour.get('clouds'),
                    'clouds_hi': hour.get('clouds_hi'),
                    'clouds_mid': hour.get('clouds_mid'),
                    'clouds_low': hour.get('clouds_low'),
                    'wind_dir': hour.get('wind_cdir'),
                    'wind_speed': hour.get('wind_spd'),
                    'humidity': hour.get('rh'),
                    'pop': hour.get('pop'),
                    'precipitation': hour.get('precip'),
                    'dewpoint': hour.get('dewpt'),
                    'vis': hour.get('vis'),
                    'snow': hour.get('snow'),
                    'app_temp': hour.get('app_temp'),
                    'temp': hour.get('temp'),
                    'description': hour.get('weather').get('description'),
                    'weather_code': hour.get('weather').get('code')
                }
            )
        days.append(day[:])
        return days

    def load_days(self, file):
        """ Load daily weather forecast conditions for the next 16 days.

            Returns
            -------
            dict:
                datetime <str>: time and date,
                clouds <int>: total clouds coverage (%),
                clouds_hi <int>: high altitude (Stratus) clouds coverage (%),
                clouds_mid <int>: middle altitude (Cumulus) clouds coverage (%),
                clouds_low <int>: low altitude (Cirrus) clouds coverage (%),
                wind_dir <str>: wind direction (in Cardinal point),
                wind_speed <float>: wind speed (m/s),
                humidity <int>: relative humidity (%),
                pop <int>: probability of precipitation (%),
                precipitation <float>: precipitation (mm),
                dewpoint <float>: dew point temperature (°C),
                vis <float>: visibility (km),
                snow <float>: snowfall (mm),
                temp <float>: temperature (°C),
                max_temp <float>: maximum temperature (°C),
                min_temp <float>: minimum temperature (°C),
                description <str>: quick weather description,
                weather_code <int>: weather code for icon
        """
        data = file.get('data')
        days = [
            {
                'datetime': day.get('valid_date'),
                'clouds': day.get('clouds'),
                'clouds_hi': day.get('clouds_hi'),
                'clouds_mid': day.get('clouds_mid'),
                'clouds_low': day.get('clouds_low'),
                'wind_dir': day.get('wind_cdir'),
                'wind_speed': day.get('wind_spd'),
                'humidity': day.get('rh'),
                'pop': day.get('pop'),
                'precipitation': day.get('precip'),
                'dewpoint': day.get('dewpt'),
                'vis': day.get('vis'),
                'snow': day.get('snow'),
                'temp': day.get('temp'),
                'max_temp': day.get('max_temp'),
                'min_temp': day.get('min_temp'),
                'description': day.get('weather').get('description'),
                'weather_code': day.get('weather').get('code'),
            } for day in data
        ]
        return days


class Astro:
    def __init__(self, weather):
        """ Initialize Astronomy object. """

        self.load_moon(weather)
        self.load_sun(weather)

        if weather.didReset:
            self.get_light_pollution(weather)

        with open(f'forecast{weather.period}.json') as json_file:
            file = load(json_file)

            self.bortle = file.get("bortle")
            self.art_brightness = file.get("art_brightness")
            self.ratio = file.get("ratio")

    def load_moon(self, weather):
        """ Load moon properties.

        For the 16 days period, moon informations are calculated at 20:30 (local-time, no-dst).
        """
        moon = MoonInfo(weather.toDMS(weather.lat),
                        weather.toDMS(weather.lon), Moon)

        self.moon = []

        if weather.period == 5:
            for day in weather.forecast:
                hours = []
                for hour in day:
                    localtime = hour.get('datetime')
                    localdate = datetime.fromisoformat(localtime).timetuple()
                    utc = hour.get('datetime_utc')
                    utcdate = datetime.fromisoformat(utc).timetuple()
                    if (h := localdate.tm_hour) < 16 and h > 5:
                        continue
                    moon.update((utcdate.tm_year, utcdate.tm_mon, utcdate.tm_mday,
                                 utcdate.tm_hour, utcdate.tm_min, utcdate.tm_sec))
                    hours.append(
                        {
                            'datetime': localtime,
                            'phase': round(moon.fractional_phase()*100, 1),
                            'age': moon.age(),
                            'magnitude': moon.magnitude(),
                            'altitude': moon.altitude()
                        }
                    )
                rise, transit, set = moon.rise_set_times('America/Toronto')

                if not isinstance(rise[1], str):
                    rise = list(rise)
                    rise[1] = datetime(
                        *rise[1]).strftime("%Y/%m/%d %H:%M:%S")
                if not isinstance(transit[1], str):
                    transit = list(transit)
                    transit[1] = datetime(
                        *transit[1]).strftime("%Y/%m/%d %H:%M:%S")
                if not isinstance(set[1], str):
                    set = list(set)
                    set[1] = datetime(
                        *set[1]).strftime("%Y/%m/%d %H:%M:%S")

                days = {
                    'hours': hours,
                    rise[0]: rise[1],
                    transit[0]: transit[1],
                    set[0]: set[1]
                }
                self.moon.append(days)

        elif weather.period == 16:
            days = []
            for day in weather.forecast:
                date = datetime.fromisoformat(
                    day.get('datetime')).timetuple()
                moon.update((date.tm_year, date.tm_mon,
                             date.tm_mday+1, 1, 30, 0))  # timezone (date + 5h)
                rise, transit, set = moon.rise_set_times('America/Toronto')
                if not isinstance(rise[1], str):
                    rise = list(rise)
                    rise[1] = datetime(
                        *rise[1]).strftime("%Y/%m/%d %H:%M:%S")
                if not isinstance(transit[1], str):
                    transit = list(transit)
                    transit[1] = datetime(
                        *transit[1]).strftime("%Y/%m/%d %H:%M:%S")
                if not isinstance(set[1], str):
                    set = list(set)
                    set[1] = datetime(
                        *set[1]).strftime("%Y/%m/%d %H:%M:%S")

                days.append(
                    {
                        'datetime': day.get('datetime'),
                        'phase': round(moon.fractional_phase()*100, 1),
                        'age': moon.age(),
                        'magnitude': moon.magnitude(),
                        'altitude': moon.altitude(),
                        rise[0]: rise[1],
                        transit[0]: transit[1],
                        set[0]: set[1]
                    }
                )
            self.moon = days

    def load_sun(self, weather):
        """ Load sun properties. """
        city = LocationInfo(weather.city_name, "Canada",
                            weather.timezone, weather.lat, weather.lon)

        self.sun = []
        if weather.period == 5:
            for day in weather.forecast:
                date = datetime.fromisoformat(
                    day[0].get('datetime')).timetuple()

                s = sun(city.observer, date=datetime(*date[:3]), dawn_dusk_depression=Depression.ASTRONOMICAL,
                        tzinfo=timezone(weather.timezone_name))
                s2 = sun(city.observer, date=datetime(*date[:3]), dawn_dusk_depression=Depression.NAUTICAL,
                         tzinfo=timezone(weather.timezone_name))
                days = {
                    'a_dawn': s['dawn'],
                    'a_dusk': s['dusk'],
                    'n_dawn': s2['dawn'],
                    'n_dusk': s2['dusk'],
                    'sunrise': s['sunrise'],
                    'sunset': s['sunset']
                }
                self.sun.append(days)

        elif weather.period == 16:
            for day in weather.forecast:
                date = datetime.fromisoformat(
                    day.get('datetime')).timetuple()

                s = sun(city.observer, date=datetime(*date[:3]), dawn_dusk_depression=Depression.ASTRONOMICAL,
                        tzinfo=timezone(weather.timezone_name))
                s2 = sun(city.observer, date=datetime(*date[:3]), dawn_dusk_depression=Depression.NAUTICAL,
                         tzinfo=timezone(weather.timezone_name))
                days = {
                    'a_dawn': s['dawn'],
                    'a_dusk': s['dusk'],
                    'n_dawn': s2['dawn'],
                    'n_dusk': s2['dusk'],
                    'sunrise': s['sunrise'],
                    'sunset': s['sunset']
                }
                self.sun.append(days)

    def bortle_scale(self, brightness):

        sqm = log10((brightness + 0.171168465)/108000000)/-0.4

        if sqm > 23 or sqm < 5:
            return ValueError("Sky brightness (SQM) cannot be higher than 23 or less than 5.")

        bortle_sky = {
            1: [23, 21.99],
            2: [21.99, 21.89],
            3: [21.89, 21.69],
            4: [21.69, 21.25],
            4.5: [21.25, 20.49],
            5: [20.49, 19.50],
            6: [19.50, 18.95],
            7: [18.95, 18.38],
            8: [18.38, 17.80],
            9: [17.80, 5]
        }

        for bortle, sky in bortle_sky.items():
            if sqm <= sky[0] and sqm >= sky[1]:
                return bortle

    def get_light_pollution(self, weather):
        url = "https://www.lightpollutionmap.info/QueryRaster/"

        querystring = {"ql": 'wa_2015', "qt": 'point',
                       "qd": f'{weather.lon},{weather.lat}', "key": "078BDzVxU2IwEaGc"}
        response = request(
            "GET", url, params=querystring)

        brightness = float(response.text)

        bortle = self.bortle_scale(brightness)
        art_brightness = round(brightness * 1000)
        ratio = round(brightness / 0.171168465, 1)

        with open(f'forecast{weather.period}.json', "r+") as json_file:
            file = load(json_file)
            file.update({
                "bortle": bortle,
                "art_brightness": art_brightness,
                "ratio": ratio
            })
            json_file.seek(0)
            dump(file, json_file)

            file["bortle"] = bortle
            file["art_brightness"] = art_brightness
            file["ratio"] = ratio
